package com.gamesite6.hanabi.domain

import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class SettingsTest {

    private val jsonString = """{ "selectedPlayerCounts" : [3,4,5] }"""

    @Test
    fun jsonTest() {
        val settings = Json.decodeFromString(Settings.serializer(), jsonString)

        assertEquals(
            settings,
            Settings(selectedPlayerCounts = setOf(3, 4, 5))
        )
    }
}