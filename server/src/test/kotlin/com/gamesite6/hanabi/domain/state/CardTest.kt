package com.gamesite6.hanabi.domain.state

import kotlin.test.Test
import kotlin.test.assertEquals

class CardTest {

    @Test
    fun testRanksAndSuits() {
        assertEquals(0.rank(), Rank.One)
        assertEquals(0.suit(), Suit.White)

        assertEquals(13.rank(), Rank.Two)
        assertEquals(13.suit(), Suit.Yellow)

        assertEquals(25.rank(), Rank.Three)
        assertEquals(25.suit(), Suit.Green)

        assertEquals(37.rank(), Rank.Four)
        assertEquals(37.suit(), Suit.Blue)

        assertEquals(49.rank(), Rank.Five)
        assertEquals(49.suit(), Suit.Red)

    }
}