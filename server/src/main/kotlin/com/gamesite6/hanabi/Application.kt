package com.gamesite6.hanabi

import com.gamesite6.hanabi.domain.Action
import com.gamesite6.hanabi.domain.PlayerId
import com.gamesite6.hanabi.domain.Settings
import com.gamesite6.hanabi.domain.State
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.serialization.json
import io.ktor.server.cio.CIO
import io.ktor.server.engine.embeddedServer
import kotlinx.serialization.Serializable


@Serializable
data class InfoReq(
    val settings: Settings
)

@Serializable
data class InfoRes(
    val playerCounts: List<Int>
)

@Serializable
data class InitialStateReq(
    val players: Set<PlayerId>,
    val settings: Settings,
    val seed: Long
)

@Serializable
data class InitialStateRes(
    val state: State
)

@Serializable
data class PerformActionReq(
    val performedBy: PlayerId,
    val action: Action,
    val state: State,
    val settings: Settings,
    val seed: Long
)

@Serializable
data class PerformActionRes(
    val completed: Boolean,
    val nextState: State
)

fun main() {
    val port = System.getenv("PORT")?.toInt() ?: 8080
    val server = embeddedServer(CIO, port, module = Application::module)
    server.start(wait = true)
}

val validPlayerCounts = setOf(2, 3, 4, 5)

fun Application.module() {
    install(ContentNegotiation) {
        json()
    }

    routing {
        post("/info") {
            val req = call.receive<InfoReq>()

            val playerCounts = req.settings.selectedPlayerCounts
                .intersect(validPlayerCounts)
                .toList()
                .sorted()

            call.respond(InfoRes(playerCounts))
        }

        post("/initial-state") {
            val req = call.receive<InitialStateReq>()
            val state = initialState(
                playerIds = req.players,
                settings = req.settings,
                seed = req.seed
            )
            if (state != null) {
                call.respond(InitialStateRes(state))
            } else {
                call.respond(HttpStatusCode.UnprocessableEntity)
            }
        }

        post("/perform-action") {
            val req = call.receive<PerformActionReq>()

            val nextState = performAction(
                state = req.state,
                settings = req.settings,
                action = req.action,
                userId = req.performedBy
            )

            if (nextState != null) {
                call.respond(
                    PerformActionRes(
                        nextState = nextState,
                        completed = nextState.isGameCompleted()
                    )
                )
            } else {
                call.respond(HttpStatusCode.UnprocessableEntity)
            }
        }
    }
}
