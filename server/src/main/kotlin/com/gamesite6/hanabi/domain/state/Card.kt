package com.gamesite6.hanabi.domain.state

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class Rank(val value: Int) {
    @SerialName("1")
    One(1),

    @SerialName("2")
    Two(2),

    @SerialName("3")
    Three(3),

    @SerialName("4")
    Four(4),

    @SerialName("5")
    Five(5)
}

@Serializable
enum class Suit {
    @SerialName("W")
    White,

    @SerialName("Y")
    Yellow,

    @SerialName("G")
    Green,

    @SerialName("B")
    Blue,

    @SerialName("R")
    Red
}

typealias Card = Int

private val ranks: List<Rank> = listOf(
    Rank.One,
    Rank.One,
    Rank.One,
    Rank.Two,
    Rank.Two,
    Rank.Three,
    Rank.Three,
    Rank.Four,
    Rank.Four,
    Rank.Five
)

private val suits: List<Suit> = listOf(
    Suit.White,
    Suit.Yellow,
    Suit.Green,
    Suit.Blue,
    Suit.Red
)

val cards: List<Triple<Card, Rank, Suit>> = suits.withIndex()
    .flatMap { (suitIndex, suit) ->
        ranks.withIndex().map { (rankIndex, rank) ->
            Triple(suitIndex * ranks.size + rankIndex, rank, suit)
        }
    }


fun Card.rank(): Rank {
    val (_, rank, _) = cards[this % cards.size]
    return rank
}

fun Card.suit(): Suit {
    val (_, _, suit) = cards[this % cards.size]
    return suit
}
