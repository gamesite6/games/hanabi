package com.gamesite6.hanabi.domain

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import com.gamesite6.hanabi.domain.state.Rank as StateRank
import com.gamesite6.hanabi.domain.state.Suit as StateSuit

@Serializable
sealed class Action {
    @Serializable
    @SerialName("Hint")
    data class Hint(val target: PlayerId, val hint: HintType) : Action()

    @Serializable
    @SerialName("Play")
    data class Play(val idx: Int) : Action()

    @Serializable
    @SerialName("Discard")
    data class Discard(val idx: Int) : Action()
}

@Serializable
sealed class HintType {
    @Serializable
    @SerialName("Suit")
    data class Suit(val suit: StateSuit) : HintType()

    @Serializable
    @SerialName("Rank")
    data class Rank(val rank: StateRank) : HintType()
}
