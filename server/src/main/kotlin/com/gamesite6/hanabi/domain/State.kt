package com.gamesite6.hanabi.domain

import com.gamesite6.hanabi.domain.state.Card
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

typealias PlayerId = Int

@Serializable
data class State(
    val players: List<Player>,
    val deck: List<Card>,
    val drewLast: PlayerId?,
    val hints: Int,
    val lives: Int,
    val played: List<Card>,
    val discarded: List<Card>,
    val phase: Phase,
    val latest: PerformedAction?
)

@Serializable
sealed class PerformedAction {
    @Serializable
    @SerialName("Hinted")
    data class Hinted(
        val player: PlayerId,
        val target: PlayerId,
        val hint: HintType,
        val amount: Int
    ) : PerformedAction()

    @Serializable
    @SerialName("Played")
    data class Played(val player: PlayerId, val card: Card, val success: Boolean) : PerformedAction()

    @Serializable
    @SerialName("Discarded")
    data class Discarded(val player: PlayerId, val card: Card) : PerformedAction()
}


fun State.modifyPlayer(playerId: PlayerId, f: (Player) -> Player): State {
    val player = this.players.find { it.id == playerId } ?: return this
    val playerIdx = this.players.indexOf(player)

    return this.copy(
        players =
        this.players.subList(0, playerIdx) +
                f(player) +
                this.players.subList(playerIdx + 1, this.players.size)
    )
}

@Serializable
data class Player(
    val id: PlayerId,
    val hand: List<CardInHand>
)

@Serializable
data class CardInHand(
    val card: Card,
    /** Whether the card rank has been hinted */
    val rankHint: Boolean,
    /** Whether the card suit has been hinted */
    val suitHint: Boolean
)

@Serializable
sealed class Phase {
    @Serializable
    @SerialName("Playing")
    data class Playing(val player: PlayerId) : Phase()

    @Serializable
    @SerialName("GameComplete")
    object GameComplete : Phase()
}