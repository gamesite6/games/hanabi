package com.gamesite6.hanabi.domain

import kotlinx.serialization.Serializable

@Serializable
data class Settings (
    val selectedPlayerCounts: Set<Int>
)
