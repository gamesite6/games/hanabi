package com.gamesite6.hanabi

import com.gamesite6.hanabi.domain.*
import com.gamesite6.hanabi.domain.state.Card
import com.gamesite6.hanabi.domain.state.Rank
import com.gamesite6.hanabi.domain.state.rank
import com.gamesite6.hanabi.domain.state.suit

private fun actionIsAllowed(
    state: State,
    @Suppress("UNUSED_PARAMETER")
    settings: Settings,
    action: Action,
    userId: PlayerId
): Boolean {
    return when (state.phase) {
        is Phase.Playing -> state.phase.player == userId && when (action) {
            is Action.Hint -> {
                state.players.any { it.id == action.target && it.id != userId } && state.hints > 0
            }

            is Action.Play -> {
                state.players.any { it.id == userId && it.hand.size > action.idx }
            }

            is Action.Discard -> {
                state.players.any { it.id == userId && it.hand.size > action.idx } && state.hints < 8
            }
        }
        Phase.GameComplete -> false
    }
}

fun State.isGameCompleted(): Boolean {
    return this.phase == Phase.GameComplete
}

fun getNextPlayer(players: List<Player>, currentPlayerId: PlayerId): Player {
    val currentPlayerIdx = players.indexOfFirst { it.id == currentPlayerId }
    val nextPlayerIdx = (currentPlayerIdx + 1) % players.size
    return players[nextPlayerIdx]
}

fun performAction(state: State, settings: Settings, action: Action, userId: PlayerId): State? {

    if (!actionIsAllowed(state, settings, action, userId)) {
        return null
    }

    val user = state.players.find { it.id == userId } ?: return null

    return when (state.phase) {
        is Phase.Playing ->
            when (action) {
                is Action.Hint -> {
                    val hintMatches = state.players.find { it.id == action.target }?.hand?.count { cardInHand ->
                        when (action.hint) {
                            HintType.Rank(cardInHand.card.rank()) -> true
                            HintType.Suit(cardInHand.card.suit()) -> true
                            else -> false
                        }
                    } ?: 0

                    state
                        .modifyPlayer(action.target) {
                            it.copy(
                                hand = it.hand.map { cardInHand ->
                                    when (action.hint) {
                                        HintType.Rank(cardInHand.card.rank()) -> cardInHand.copy(rankHint = true)
                                        HintType.Suit(cardInHand.card.suit()) -> cardInHand.copy(suitHint = true)
                                        else -> cardInHand
                                    }
                                }
                            )
                        }
                        .copy(
                            hints = state.hints - 1,
                            phase =
                            if (state.drewLast == userId)
                                Phase.GameComplete
                            else Phase.Playing(
                                player = getNextPlayer(
                                    state.players,
                                    userId
                                ).id
                            ),
                            latest = PerformedAction.Hinted(
                                player = userId,
                                target = action.target,
                                hint = action.hint,
                                amount = hintMatches
                            )

                        )
                }

                is Action.Play -> {
                    val playedCard = user.hand[action.idx].card
                    val drawnCard = state.deck.firstOrNull()
                    val newDeck = state.deck.drop(1)

                    val grouped = state.played.groupBy(keySelector = { it.suit() }, valueTransform = { it.rank() })
                    val previouslyPlayedOfSuit = grouped[playedCard.suit()]?.size ?: 0
                    val successful = playedCard.rank().value == previouslyPlayedOfSuit + 1
                    val nextLives = if (!successful) state.lives - 1 else state.lives
                    val nextHints =
                        if (successful && playedCard.rank() == Rank.Five) (state.hints + 1).coerceAtMost(8)
                        else state.hints

                    var nextState = state.modifyPlayer(userId) { p ->
                        p.copy(
                            hand = p.hand.filterNot { it.card == playedCard } +
                                    if (drawnCard != null)
                                        listOf(
                                            CardInHand(
                                                drawnCard,
                                                rankHint = false,
                                                suitHint = false
                                            )
                                        )
                                    else
                                        emptyList()
                        )
                    }
                        .copy(
                            deck = newDeck,

                            lives = nextLives,
                            hints = nextHints,
                            played = if (successful) state.played + playedCard else state.played,
                            discarded = if (!successful) state.discarded + playedCard else state.discarded
                        )

                    if (state.deck.isNotEmpty() && newDeck.isEmpty()) {
                        nextState = nextState.copy(drewLast = userId)
                    }

                    if (nextLives == 0) {
                        nextState.copy(
                            phase = Phase.GameComplete
                        )
                    } else {
                        nextState.copy(
                            phase =
                            if (state.drewLast == userId)
                                Phase.GameComplete
                            else
                                Phase.Playing(
                                    player = getNextPlayer(
                                        state.players,
                                        userId
                                    ).id
                                )
                        )
                    }.copy(
                        latest = PerformedAction.Played(
                            player = userId,
                            card = playedCard,
                            success = successful
                        )
                    )
                }
                is Action.Discard -> {
                    val discardedCard: Card = user.hand[action.idx].card
                    val drawnCard = state.deck.firstOrNull()
                    val newDeck = state.deck.drop(1)

                    var nextState = state
                        .modifyPlayer(userId) { p ->
                            p.copy(hand = p.hand.filterNot { it.card == discardedCard } +
                                    if (drawnCard != null)
                                        listOf(
                                            CardInHand(
                                                drawnCard,
                                                rankHint = false,
                                                suitHint = false
                                            )
                                        )
                                    else
                                        emptyList()
                            )
                        }
                        .copy(
                            deck = newDeck,
                            hints = state.hints + 1,
                            discarded = state.discarded + discardedCard,
                            phase =
                            if (state.drewLast == userId)
                                Phase.GameComplete
                            else
                                Phase.Playing(
                                    player = getNextPlayer(
                                        state.players,
                                        userId
                                    ).id
                                ),
                            latest = PerformedAction.Discarded(
                                player = userId,
                                card = discardedCard
                            )
                        )

                    if (state.deck.isNotEmpty() && newDeck.isEmpty()) {
                        nextState = nextState.copy(drewLast = userId)
                    }

                    nextState
                }
            }
        is Phase.GameComplete -> null
    }


}
