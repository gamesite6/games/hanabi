package com.gamesite6.hanabi

import com.gamesite6.hanabi.domain.*
import com.gamesite6.hanabi.domain.state.cards
import kotlin.random.Random

fun initialState(playerIds: Set<PlayerId>, settings: Settings, seed: Long): State? {
    if (!settings.selectedPlayerCounts.contains(playerIds.size)) {
        return null
    }

    val rng = Random(seed)

    var deck = cards.map { (card, _, _) -> card }.shuffled(rng)

    val cardsPerPlayer = when (playerIds.size) {
        2, 3 -> 5
        4, 5 -> 4
        else -> return null
    }

    val players = playerIds.map { playerId ->
        val player =
            Player(
                id = playerId,
                hand = deck.subList(0, cardsPerPlayer).map { card ->
                    CardInHand(card, rankHint = false, suitHint = false)
                })
        deck = deck.subList(cardsPerPlayer, deck.size)
        player
    }.shuffled(rng)

    return State(
        players = players,
        deck = deck,
        drewLast = null,
        hints = 8,
        lives = 3,
        played = emptyList(),
        discarded = emptyList(),
        phase = Phase.Playing(playerIds.first()),
        latest = null
    )
}
