import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
    }
}

plugins {
    application

    kotlin("jvm") version "1.6.10"
    kotlin("plugin.serialization") version "1.6.10"

    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("com.github.ben-manes.versions") version "0.40.0"
}

group = "com.gamesite6"
version = "0.1.0"

application {
    mainClass.set("com.gamesite6.hanabi.ApplicationKt")

    applicationDefaultJvmArgs =
        if (System.getenv("DEVELOPMENT") == "true")
            listOf("-Dio.ktor.development=true")
        else
            listOf()

}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("ch.qos.logback:logback-classic:1.2.10")
    implementation("io.ktor:ktor-server-cio:1.6.7")
    implementation("io.ktor:ktor-server-core:1.6.7")
    implementation("io.ktor:ktor-serialization:1.6.7")
    testImplementation(kotlin("test"))
    testImplementation("io.ktor:ktor-server-tests:1.6.7")
}
