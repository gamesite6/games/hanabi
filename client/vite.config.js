import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/fireworks/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/main.ts",
      fileName: "fireworks",
      name: "Gamesite6_Fireworks",
    },
  },
  plugins: [svelte()],
});
