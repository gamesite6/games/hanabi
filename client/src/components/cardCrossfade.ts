import { crossfade } from "svelte/transition";

export let [send, receive] = crossfade({
  duration: (d) => Math.sqrt(d * 500),
});
