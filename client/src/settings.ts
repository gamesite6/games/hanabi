export const defaultSettings: GameSettings = {
  selectedPlayerCounts: [2, 3, 4, 5],
};

export const validPlayerCounts = [2, 3, 4, 5];

export function playerCounts(settings: GameSettings): number[] {
  return validPlayerCounts.filter((c) =>
    settings.selectedPlayerCounts.includes(c)
  );
}
