import { getRank, getSuit } from "./serverUtils";

test("card ranks and suits", () => {
  expect(getRank(0)).toEqual(1);
  expect(getSuit(0)).toEqual("W");

  expect(getRank(13)).toEqual(2);
  expect(getSuit(13)).toEqual("Y");

  expect(getRank(25)).toEqual(3);
  expect(getSuit(25)).toEqual("G");

  expect(getRank(37)).toEqual(4);
  expect(getSuit(37)).toEqual("B");

  expect(getRank(49)).toEqual(5);
  expect(getSuit(49)).toEqual("R");
});
