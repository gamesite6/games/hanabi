import type { Phase, PlayerId, Action } from "../lib/server";
import { createEventDispatcher } from "svelte";

export type ActionEvent = CustomEvent<Action>;

export function createActionDispatcher(): (action: Action) => void {
  const dispatchEvent = createEventDispatcher();
  return (action) => {
    dispatchEvent("action", action);
  };
}

export function isActivePlayer(phase: Phase, playerId: PlayerId): boolean {
  switch (phase[0]) {
    case "Playing":
      return phase[1].player === playerId;
    case "GameComplete":
      return false;
  }
}

export enum ActionMode {
  Discarding = "Discarding",
  Hinting = "Hinting",
  Playing = "Playing",
}
