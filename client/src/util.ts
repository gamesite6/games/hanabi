import { getSuit, getRank } from "./serverUtils";

export function getHighestPerSuit(played: Card[]): Card[] {
  const highest = new Map<Suit, Card>();

  for (const card of played) {
    const suit = getSuit(card);
    const rank = getRank(card);
    const previousHigh = highest.get(suit);
    if (previousHigh === undefined || rank > getRank(previousHigh)) {
      highest.set(suit, card);
    }
  }
  const values = Array.from(highest.values());
  sortAscending(values);
  return values;
}

export function groupCardsBySuit(played: Card[]): Card[][] {
  const sortedCards = [...played];
  sortAscending(sortedCards);

  return groupAdjacent(sortedCards, (a, b) => getSuit(a) === getSuit(b));
}

export function cardsBySuit(cards: Card[]): { [suit: string]: Card[] } {
  let groups = groupCardsBySuit(cards);
  let result: { [suit: string]: Card[] } = {};
  for (let group of groups) {
    const suit = getSuit(group[0]);
    result[suit] = group;
  }
  return result;
}

export function groupAdjacent<T>(
  arr: T[],
  isEqual: (a: T, b: T) => boolean = (a, b) => a === b
): T[][] {
  if (arr.length > 0) {
    let previousValue = arr[0];
    let currentArray = [previousValue];
    const result: T[][] = [currentArray];

    for (let i = 1; i < arr.length; i++) {
      const currentValue = arr[i];
      if (isEqual(previousValue, currentValue)) {
        previousValue = currentValue;
        currentArray.push(currentValue);
      } else {
        previousValue = currentValue;
        currentArray = [previousValue];
        result.push(currentArray);
      }
    }
    return result;
  } else {
    return [];
  }
}

export function orderPlayers(
  players: Player[],
  user: PlayerId | null
): Player[] {
  const userIdx = players.findIndex((p) => p.id === user);
  if (userIdx !== -1) {
    return [...players.slice(userIdx), ...players.slice(0, userIdx)];
  } else {
    return players;
  }
}

export function sortAscending(arr: number[]): void {
  arr.sort((a, b) => a - b);
}

export function sortDescending(arr: number[]): void {
  arr.sort((a, b) => b - a);
}

export function reversed<T>(arr: T[]): T[] {
  const result = [...arr];
  result.reverse();
  return result;
}
