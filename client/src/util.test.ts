import { groupAdjacent } from "./util";

describe("groupAdjacent", () => {
  test("groups numbers", () => {
    expect(groupAdjacent([1, 1, 2, 2, 3, 3])).toEqual([
      [1, 1],
      [2, 2],
      [3, 3],
    ]);
  });
  test("groups sequence into single element groups", () => {
    expect(groupAdjacent([1, 2, 3, 4, 5])).toEqual([[1], [2], [3], [4], [5]]);
  });
  test("no groups for empty array", () => {
    expect(groupAdjacent([])).toEqual([]);
  });
});
