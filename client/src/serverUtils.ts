export const suits: Suit[] = ["W", "Y", "G", "B", "R"];

const ranks: Rank[] = [1, 1, 1, 2, 2, 3, 3, 4, 4, 5];

const cards: [Card, Rank, Suit][] = [];
suits.forEach((suit, suitIndex) => {
  ranks.forEach((rank, rankIndex) => {
    const card = suitIndex * ranks.length + rankIndex;
    cards[card] = [card, rank, suit];
  });
});

export function getRank(card: Card): Rank {
  const [_card, rank, _suit] = cards[card];
  return rank;
}
export function getSuit(card: Card): Suit {
  const [_card, _rank, suit] = cards[card];
  return suit;
}
