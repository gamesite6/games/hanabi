type Card = number;

type Suit = "W" | "Y" | "B" | "G" | "R";

type Rank = 1 | 2 | 3 | 4 | 5;

type PlayerId = number;

type GameSettings = { selectedPlayerCounts: number[] };

type GameState = {
  players: Player[];
  deck: Card[];
  drewLast: PlayerId | null;
  hints: number;
  lives: number;
  played: Card[];
  discarded: Card[];
  phase: Phase;
  latest: PerformedAction | null;
};

type Phase = ["Playing", { player: PlayerId }] | ["GameComplete"];

type PerformedAction =
  | ["Played", { player: number; card: Card; success: boolean }]
  | ["Discarded", { player: number; card: Card }]
  | [
      "Hinted",
      { player: number; target: PlayerId; hint: Hint; amount: number }
    ];

type Player = {
  id: PlayerId;
  hand: CardInHand[];
};

type CardInHand = {
  card: Card;
  rankHint: boolean;
  suitHint: boolean;
};

type Action =
  | ["Reorder", { order: Card[] }]
  | ["Play", { idx: number }]
  | ["Discard", { idx: number }]
  | ["Hint", { target: PlayerId; hint: Hint }];

type Hint = ["Suit", { suit: Suit }] | ["Rank", { rank: Rank }];
